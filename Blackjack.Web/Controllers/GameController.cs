﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Blackjack.BusinessLogic.Services.Interfaces;
using Blackjack.ViewModels.GameViews;
using NLog;

namespace Blackjack.Web.Controllers
{
    public class GameController : AsyncController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private IGameService _gameService;

        public GameController(IGameService gameService) {
            _gameService = gameService;
        }
       
        [HttpGet]
        public async Task<ActionResult> Start()
        { 
            try
            {
                StartGameView game = await _gameService.Start();
                return View(game);
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return View("Error");
            }
        }

        [HttpGet]
        public async Task<JsonResult> StartDealers()
        {
            try
            {
                var dealers = await _gameService.StartDealers();
                return Json(dealers.DealersList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return Json(e, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public async Task<JsonResult> StartPlayers()
        {
            try
            {
                var players = await _gameService.StartPlayers();
                return Json(players.PlayersList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return Json(e, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Start(StartGameView startGame)
        {
            try
            {
                var gameId = await _gameService.Start(startGame);
                return RedirectToAction("Play", new { id = gameId });
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return View("Error");
            }            
        }
        
        [HttpGet]
        public async Task<ActionResult> Play(long id)
        {
            try
            {
                PlayGameView game = await _gameService.Play(id);
                return View(game);
            }
            catch(Exception e)
            {
                logger.Error(e.Message);
                return View("Error");
            }            
        }

        [HttpPost]
        public async Task<ActionResult> Enough(long gameId)
        {
            try
            {
                await _gameService.Enough(gameId);
                return RedirectToAction("Details", new { id = gameId });
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return View("Error");
            } 
        }

        [HttpPost]
        public async Task<ActionResult> More(long gameId)
        {
            try
            {
                await _gameService.More(gameId);
                return RedirectToAction("Play", new { id = gameId });
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return View("Error");
            }
        }

        [HttpGet]
        public async Task<ActionResult> History()
        {
            try
            {
                var games = await _gameService.History();
                return View(games);
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return View("Error");
            }
        }

        [HttpGet]
        public async Task<ActionResult> Details(long id) {
            try
            {
                DetailsGameView game = await _gameService.Details(id);
                return View(game);
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return View("Error");
            }
        }
    }
}