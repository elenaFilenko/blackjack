﻿var templateLoader = (function ($, host) {
    return {
        loadExtTemplate: function (path) {
            var tmplLoader = $.get(path)
                .done(function (result) {
                    $("body").append(result);
                })
                .error(function (result) {
                    alert("Error Loading Templates -- TODO: Better Error Handling");
                })

            tmplLoader.complete(function () {
                $(host).trigger("TEMPLATE_LOADED", [path]);
            });
        }
    };
})(jQuery, document);
