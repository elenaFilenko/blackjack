﻿$(document).bind("TEMPLATE_LOADED", function (e, path) {
    console.log('Templates loaded');
    _itemTemplate = kendo.template($("#noDataTemplate").html(), { useWithBlock: false });
    _itemTemplate(data);
});
function addNew(widgetId, value) {
    if (confirm("Are you sure?")) {
        $("#NewPlayerName").val(value);
        $("#Players").val(0);
    }
}
$(document).ready(function () {
    var dealersDataSource = new kendo.data.DataSource({
        batch: true,
        transport: {
            read: {
                url: "http://localhost:50183/Game/StartDealers",
                dataType: "json"
            }
        },
        schema: {
            model: {
                Id: "Id",
                fields: {
                    Id: { type: "number" },
                    Name: { type: "string" }
                }
            }
        }
    });
    var playersDataSource = new kendo.data.DataSource({
        batch: true,
        transport: {
            read: {
                url: "http://localhost:50183/Game/StartPlayers",
                dataType: "json"
            }
        },
        schema: {
            model: {
                Id: "Id",
                fields: {
                    Id: { type: "number" },
                    Name: { type: "string" }
                }
            }
        }
    });
    $("#Dealers").kendoDropDownList({
        dataTextField: "Name",
        dataValueField: "Id",
        dataSource: dealersDataSource
    });

    $("#Players").kendoComboBox({
        filter: "startswith",
        dataTextField: "Name",
        dataValueField: "Id",
        dataSource: playersDataSource,
        noDataTemplate: $("#noDataTemplate").html()
    });
});