﻿using System.Threading.Tasks;
using Blackjack.ViewModels.GameViews;

namespace Blackjack.BusinessLogic.Services.Interfaces
{
    public interface IGameService
    {
        Task<StartGameView> Start();
        Task<long> Start(StartGameView startGameView);
        Task<PlayGameView> Play(long id);
        Task<HistoryGameView> History();
        Task<DetailsGameView> Details(long id);
        Task<StartDealersGameView> StartDealers();
        Task<StartPlayersGameView> StartPlayers();
        Task Enough(long id);
        Task More(long id);
    }
}
