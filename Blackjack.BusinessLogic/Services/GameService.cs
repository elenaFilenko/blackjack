﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Blackjack.BusinessLogic.Services.Interfaces;
using Blackjack.Entities.Enums;
using Blackjack.Entities.Entities;
using Blackjack.ViewModels.GameViews;
using Blackjack.DataAccess.Interfaces;

namespace Blackjack.BusinessLogic.Services
{
    public class GameService : IGameService
    {   
        private static Random _rnd = new Random();
        private IGameRepository _gameRepository;
        private IPlayerRepository _playerRepository;
        private IGamePlayerRepository _gamePlayerRepository;
        private ICardRepository _cardRepository;
        private Card[] _cards;
        
        public GameService(IGameRepository gameRepository, IGamePlayerRepository gamePlayerRepository,
            IPlayerRepository playerRepository, ICardRepository cardRepository)
        {            
            _gameRepository = gameRepository;
            _gamePlayerRepository = gamePlayerRepository;
            _playerRepository = playerRepository;
            _cardRepository= cardRepository;
        }
        
        public async Task CheckGameRoundResults(long id)
        {
            var gamePlayers = (await _gamePlayerRepository.GetWithoutDealerByGameId(id)).ToList();
            foreach(var gp in gamePlayers)
            {
                if (gp.Points == AppConfig.BlackjackPointNumber && gp.Result!=GameResult.Blackjack)
                {
                    gp.Result=GameResult.Blackjack;
                }
            }
            await _gamePlayerRepository.Update(gamePlayers);
        }

        public async Task Enough(long id)
        {
            var gamePlayers = (await _gamePlayerRepository.GetWithoutDealerByGameId(id)).ToList();
            var dealer = await _gamePlayerRepository.GetDealerByGameId(id);
            var card = new Card();
            while (dealer.Points < AppConfig.DealerPointsStopNumber)
            {
                card = await PassCard();
                StartGameRound(dealer, card);
            }
            await _gamePlayerRepository.Update(dealer);
            await CheckGameRoundResults(id);
            await FinishGame(id);
        }

        private async Task FinishGame(long id)
        {
            var players = (await _gamePlayerRepository.GetWithoutDealerByGameId(id)).ToList();
            var dealer = await _gamePlayerRepository.GetDealerByGameId(id);
            foreach (var gp in players)
            {
                if (gp.Points <= AppConfig.BlackjackPointNumber && (gp.Points > dealer.Points || dealer.Points > AppConfig.BlackjackPointNumber))
                {
                    gp.Result = GameResult.Won;
                    continue;
                }
                if (gp.Points <= AppConfig.BlackjackPointNumber && gp.Points == dealer.Points)
                {
                    gp.Result = GameResult.Even;
                    continue;
                }
                gp.Result = GameResult.Lost;
            }
            await _gamePlayerRepository.Update(players);
        }

        private async Task StartFirstGameRound(long id)
        {
            var gamePlayers =( await _gamePlayerRepository.GetAllByGameId(id)).ToList();
            foreach(var gp in gamePlayers)
            {
                var firstCard = await PassCard();
                var secondCard = await PassCard();
                StartGameRound(gp, firstCard);
                StartGameRound(gp, secondCard);
            }
            await _gamePlayerRepository.Update(gamePlayers);
            await CheckGameRoundResults(id);
        }
        
        private void StartGameRound(GamePlayer gamePlayer, Card passCard)
        {
            bool valueChanged = false;
            if((gamePlayer.Points > AppConfig.AceChangeValueNumber && passCard.CardName ==CardName.Ace ) && (!valueChanged))
            {
                gamePlayer.Points += 1;
                valueChanged = true;
            }
            if(!valueChanged)
            {
                gamePlayer.Points += passCard.Value;
            }
        }

        private async Task<Card> PassCard()
        {
            if (_cards == null)
            {
                _cards= (await _cardRepository.All()).ToArray();
            }
            int cardRandomIndex = _rnd.Next(0, (_cards.Length - 1));
            var card = _cards[cardRandomIndex];
            return card;
        }

        public async Task More(long id)
        {
            var listGamePlayers =( await _gamePlayerRepository.GetWithoutDealerByGameId(id)).ToList();
            var gamePlayers = listGamePlayers.Where(l => (l.Result == GameResult.More && l.Points <= AppConfig.BlackjackPointNumber)).ToList();
            foreach (var gp in gamePlayers)
            {
                var card = await PassCard();
                StartGameRound(gp, card);
            }
            await _gamePlayerRepository.Update(gamePlayers);
            await CheckGameRoundResults(id);
        }

        public async Task<StartGameView> Start()
        {
            StartGameView game = new StartGameView();
            var players = await _playerRepository.GetPlayers();
            var dealers = await _playerRepository.GetDealers();
            game.Dealers = dealers.Select(x => new PlayerStartGameViewItem()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            game.Players = players.Select(x => new PlayerStartGameViewItem()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            return game;
        }

        public async Task<long> Start(StartGameView startGameView)
        {
            Game game = new Game();           
            game.Name = startGameView.Name!=null ? startGameView.Name: AppConfig.GameDefaultName;
            var gameId = await _gameRepository.Add(game);
            GamePlayer dealer = new GamePlayer();
            dealer.GameId = gameId;
            dealer.PlayerId = (long)startGameView.DealerId;
            dealer.Points = 0;
            dealer.Result = GameResult.More;
            await _gamePlayerRepository.Add(dealer);
            if (startGameView.PlayerId == 0 || startGameView.PlayerId == null)
            {
                var newPlayer = new Player();                
                newPlayer.Name = startGameView.NewPlayerName != null ? startGameView.NewPlayerName : AppConfig.PlayerDefaultName + gameId;
                newPlayer.Role = PlayerRole.Player;
                startGameView.PlayerId = await _playerRepository.Add(newPlayer);
            }
            GamePlayer player = new GamePlayer();
            player.GameId = gameId;
            player.PlayerId =(long)startGameView.PlayerId;
            player.Points = 0;
            player.Result = GameResult.More;
            await _gamePlayerRepository.Add(player);
            var bots = await _playerRepository.GetBots();
            var gameBots =bots.Take(startGameView.BotsNumber).Select(x => new GamePlayer()
            {
                GameId = gameId,
                PlayerId = x.Id,
                Points = 0,
                Result = GameResult.More
            }).ToList();
            await _gamePlayerRepository.Add(gameBots);
            await StartFirstGameRound(gameId);
            return gameId;
        }

        public async Task<PlayGameView> Play(long id)
        {
            PlayGameView playGameView = new PlayGameView();
            playGameView.Id = id;
            var game= await _gameRepository.GetById(id);
            playGameView.Name = game.Name;
            playGameView.CreationDate = game.CreationDate;
            var allGamePlayers = await _gamePlayerRepository.GetAllByGameId(id);
            playGameView.Players = allGamePlayers.Where(a => (!a.Player.Role.Equals(PlayerRole.Dealer)))
                .Select(a => new GamePlayerPlayGameViewItem()
            {
                Id=a.Id,
                Name=a.Player.Name,
                PlayerId=a.PlayerId,
                GameId=id,
                Points=a.Points,
                Result=a.Result.ToString()

            }).ToList();
            playGameView.Dealer = allGamePlayers.Where(a => (a.Player.Role.Equals(PlayerRole.Dealer)))
                .Select(a => new GamePlayerPlayGameViewItem()
            {
                Id = a.Id,
                Name = a.Player.Name,
                PlayerId = a.PlayerId,
                GameId = id,
                Points = a.Points,
                Result = a.Result.ToString()

            }).FirstOrDefault();
            return playGameView;
        }

        public async Task<HistoryGameView> History()
        {
            HistoryGameView history = new HistoryGameView();
            var games = await _gameRepository.GetAll();
            foreach(var game in games)
            {
                GameHistoryGameViewItem historyGame = new GameHistoryGameViewItem();
                historyGame.Id = game.Id;
                historyGame.Name = game.Name;
                historyGame.CreationDate = game.CreationDate;
                historyGame.PlayerName = game.GamePlayers
                    .Where(x => (x.Player.Role.Equals(PlayerRole.Player)))
                    .Select(x => x.Player.Name).FirstOrDefault();
                historyGame.DealerName = game.GamePlayers
                    .Where(x => (x.Player.Role.Equals(PlayerRole.Dealer)))
                    .Select(x => x.Player.Name).FirstOrDefault();
                history.Games.Add(historyGame);
            }
            return history;
        }

        public async Task<DetailsGameView> Details(long id)
        {
            DetailsGameView detailsGameView = new DetailsGameView();
            var game = await _gameRepository.GetById(id );
            detailsGameView.Id = game.Id;
            detailsGameView.Name = game.Name;
            detailsGameView.CreationDate = game.CreationDate;
            var gamePlayers = await _gamePlayerRepository.GetAllByGameId(id);
            detailsGameView.Dealer = gamePlayers.Where(gp => (gp.Player.Role.Equals(PlayerRole.Dealer)))
                .Select(gp => new GamePlayerDetailsGameViewItem() {
                    Id=gp.Id,
                    Name=gp.Player.Name,
                    GameId=id,
                    PlayerId=gp.PlayerId,
                    Points=gp.Points,
                    Result=gp.Result.ToString()
                }).FirstOrDefault();
            detailsGameView.Players= gamePlayers.Where(gp => (!gp.Player.Role.Equals(PlayerRole.Dealer)))
                .Select(gp => new GamePlayerDetailsGameViewItem()
                {
                    Id = gp.Id,
                    Name = gp.Player.Name,
                    GameId = id,
                    PlayerId = gp.PlayerId,
                    Points = gp.Points,
                    Result = gp.Result.ToString()
                }).ToList();
            return detailsGameView;
        }

        public async Task<StartDealersGameView> StartDealers()
        {
            StartDealersGameView startDealers = new StartDealersGameView();
            var dealers = await _playerRepository.GetDealers();
            startDealers.DealersList = dealers.Select(x => new PlayerStartDealersGameViewItem()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            return startDealers;
        }

        public async Task<StartPlayersGameView> StartPlayers()
        {
            StartPlayersGameView startPlayers = new StartPlayersGameView();
            var players = await _playerRepository.GetPlayers();
            startPlayers.PlayersList= players.Select(x => new PlayerStartPlayersGameViewItem()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            return startPlayers;
        }
    }
}
