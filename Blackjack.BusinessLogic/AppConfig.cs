﻿namespace Blackjack.BusinessLogic
{
    public static class AppConfig
    {
        public static int BlackjackPointNumber = 21;
        public static int DealerPointsStopNumber = 17;
        public static int AceChangeValueNumber = 11;
        public static string GameDefaultName = "Game Default name";
        public static string PlayerDefaultName = "Guest";
    }
}
