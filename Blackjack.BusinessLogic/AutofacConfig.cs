﻿using Autofac;
using Blackjack.BusinessLogic.Services.Interfaces;
using Blackjack.BusinessLogic.Services;

namespace Blackjack.BusinessLogic
{
    public static class AutofacConfig
    {
        public  static void Configure(ContainerBuilder builder, string connectionString) {
            builder.RegisterType<GameService>().As<IGameService>();           
            DataAccess.AutofacConfig.Configure(builder, connectionString);
        }
    }
}
