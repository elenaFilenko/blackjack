import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { DetailsGameView } from 'src/app/shared/models/details-game-view.model';
import { GameService } from 'src/app/shared/services/game.service';

@Component({
  selector: 'game-result',
  templateUrl: './game-result.component.html',
  styleUrls: ['./game-result.component.css'],
  providers: [GameService]
})

export class GameResultComponent implements OnInit {
  private data: DetailsGameView;
  private id: number;

  constructor(private gameService: GameService, private activatedRoute: ActivatedRoute, private router: Router) { }

  public back():void {
    this.router.navigate(['/history']);
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => { this.id = params['id']; });
    this.gameService.getDetails(this.id).subscribe((data) => {
      this.data = data;
   });
  }
}
