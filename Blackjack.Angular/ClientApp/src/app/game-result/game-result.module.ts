import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { SharedModule } from "src/app/shared/modules/shared.module";
import { GameResultRoutingModule } from "src/app/game-result/game-result-routing.module";
import { GameResultComponent } from "src/app/game-result/game-result/game-result.component";

@NgModule({
  imports: [SharedModule, HttpClientModule, GameResultRoutingModule],
  declarations: [GameResultComponent]
})

export class GameResultModule {}
