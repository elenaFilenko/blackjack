import { Injectable } from '@angular/core';
import { Http} from '@angular/http';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

import { HistoryGameView } from 'src/app/shared/models/history-game-view.model';
import { StartGameView } from 'src/app/shared/models/start-game-view.model';
import { DetailsGameView } from 'src/app/shared/models/details-game-view.model';
import { PlayGameView } from 'src/app/shared/models/play-game-view.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class GameService {
  constructor(private http: HttpClient) {}

  public getHistory(): Observable<HistoryGameView> {
    return this.http.get<HistoryGameView>(environment.baseUrl + "history");
  }

  public getDetails(id: number): Observable<DetailsGameView> {
    return this.http.get<DetailsGameView>(environment.baseUrl + "details/" + id);
  }

  public getPlay(id: number): Observable<PlayGameView> {
    return this.http.get<PlayGameView>(environment.baseUrl + "play/" + id);
  }

  public postMore(id: number): Observable<string> {
    return this.http.put<string>(environment.baseUrl + "more/" + id, null);
  }

  public postEnough(id: number): Observable<string> {
    return this.http.put<string>(environment.baseUrl + "enough/" + id, null);
  }

  public getStart(): Observable<StartGameView> {
    return this.http.get<StartGameView>(environment.baseUrl + "start");
  }

  public postStart(start: StartGameView): Observable<string> {
    return this.http.post<string>(environment.baseUrl + "start", start);
  }
}
