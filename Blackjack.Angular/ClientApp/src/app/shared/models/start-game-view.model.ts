export interface StartGameView {
  name: string;
  dealers: PlayerStartGameViewItem[];
  players: PlayerStartGameViewItem[];
  dealerId: number;
  playerId: number;
  newPlayerName: string;
  botsNumber: number;
}

export interface PlayerStartGameViewItem {
  id: number;
  name: string;
}
