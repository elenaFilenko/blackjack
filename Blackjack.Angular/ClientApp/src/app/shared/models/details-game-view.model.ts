export interface DetailsGameView {
  id: number;
  name: string;
  creationDate: Date;
  dealer: GamePlayerDetailsGameViewItem;
  players: GamePlayerDetailsGameViewItem[];
}
export interface GamePlayerDetailsGameViewItem {
  id: number;
  gameId: number;
  playersId: number;
  name: string;
  points: number;
  result: string;
}
