export interface HistoryGameView {
  games: GameHistoryGameViewItem[]
}

export interface GameHistoryGameViewItem {
  id: number;
  name: string;
  dealerName: string;
  playerName: string;
  creationDate: Date;
}
