export interface PlayGameView {
  id: number;
  name: string;
  creationDate: Date;
  dealer: GamePlayerPlayGameViewItem;
  players: GamePlayerPlayGameViewItem[];
}

export interface GamePlayerPlayGameViewItem {
  id: number;
  gameId: number;
  playersId: number;
  name: string;
  points: number;
  result: string;
}
