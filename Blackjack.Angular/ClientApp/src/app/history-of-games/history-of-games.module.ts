import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { SharedModule } from "src/app/shared/modules/shared.module";
import { HistoryRoutingModule } from "src/app/history-of-games/history-of-games-routing.module";
import { HistoryComponent } from "src/app/history-of-games/history-of-games/history-of-games.component";

@NgModule({
  imports: [
    SharedModule,
    HttpClientModule,
    HistoryRoutingModule],
  declarations: [HistoryComponent],
  providers: []
})

export class HistoryModule {}
