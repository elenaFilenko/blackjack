import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { HistoryGameView } from 'src/app/shared/models/history-game-view.model';
import { GameService } from 'src/app/shared/services/game.service';

@Component({
  selector: 'history',
  templateUrl: './history-of-games.component.html',
  styleUrls: ['./history-of-games.component.css'],
  providers: [GameService]
})

export class HistoryComponent implements OnInit {
  private data: HistoryGameView;

  constructor(private gameService: GameService, private router: Router) { }

  ngOnInit() {
    this.gameService.getHistory().subscribe((data) => {
      this.data = data;
    });
  }

  public showResults(id: number): void {
    this.router.navigate(['/result', id]);
  }

  public startGame(): void {
    this.router.navigate(['/start']);
  }
}
