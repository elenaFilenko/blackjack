import { NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'history', loadChildren: 'src/app/history-of-games/history-of-games.module#HistoryModule' },
  { path: 'result/:id', loadChildren: 'src/app/game-result/game-result.module#GameResultModule' },
  { path: 'play/:id', loadChildren: 'src/app/play-game/play-game.module#PlayGameModule' },
  { path: 'start', loadChildren: 'src/app/start-game/start-game.module#StartGameModule'},
  { path: '', redirectTo: 'start', pathMatch: 'full' },
  { path: '**', loadChildren: 'src/app/error/error.module#ErrorModule'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
