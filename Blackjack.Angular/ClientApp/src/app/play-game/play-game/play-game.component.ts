import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { GameService } from "src/app/shared/services/game.service";
import { PlayGameView } from "src/app/shared/models/play-game-view.model";

@Component({
  selector: 'play-game',
  templateUrl: './play-game.component.html',
  styleUrls: ['./play-game.component.css'],
  providers: [GameService]
})

export class PlayGameComponent implements OnInit {
  private data: PlayGameView;
  private id: number;

  constructor(private gameService: GameService, private activatedRoute: ActivatedRoute, private router: Router) { }

  public enough(): void {
    this.gameService.postEnough(this.data.id).subscribe(response => {
      let id = this.data.id;
      this.router.navigate(['/result', id]);
    });
  }

  public more(): void {
    this.gameService.postMore(this.data.id).subscribe(response => {
      this.gameService.getPlay(this.data.id).subscribe((data) => {
        this.data.dealer = data['dealer'];
        this.data.players = data['players'];
      });
    });
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => { this.id = params['id']; });
    this.gameService.getPlay(this.id).subscribe((data) => {
      this.data = data;
    });
  }
}
