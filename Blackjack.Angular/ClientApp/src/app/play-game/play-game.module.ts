import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { SharedModule } from "src/app/shared/modules/shared.module";
import { PlayGameRoutingModule } from "src/app/play-game/play-game-routing.module";
import { PlayGameComponent } from "src/app/play-game/play-game/play-game.component";

@NgModule({

  imports: [SharedModule, HttpClientModule, PlayGameRoutingModule],
  declarations: [PlayGameComponent],
})

export class PlayGameModule {}
