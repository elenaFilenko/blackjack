import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { SharedModule } from "src/app/shared/modules/shared.module";
import { StartGameRoutingModule } from "src/app/start-game/start-game-routing.module";
import { StartGameComponent } from "src/app/start-game/start-game/start-game.component";

@NgModule({
  imports: [SharedModule, HttpClientModule, StartGameRoutingModule],
  declarations: [StartGameComponent]
})

export class StartGameModule {}
