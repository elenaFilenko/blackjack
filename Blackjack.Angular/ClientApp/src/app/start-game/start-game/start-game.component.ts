import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Observable } from "rxjs";

import { GameService } from "src/app/shared/services/game.service";
import { StartGameView } from "src/app/shared/models/start-game-view.model";

@Component({
  selector: 'start-game',
  templateUrl: './start-game.component.html',
  styleUrls: ['./start-game.component.css'],
  providers:[GameService]
})

export class StartGameComponent implements OnInit{
  private data: StartGameView;
  private gameId: string;
  private noNewPlayer: boolean;
 
  constructor(private gameService: GameService, private router: Router) { }

  public addPlayer(): void {
    this.noNewPlayer = false;
    this.data.playerId = 0;
  }

  public onSubmit(): void {
    this.gameService.postStart(this.data).subscribe(response => {
      let id = response;
      this.router.navigate(['/play', id]);
    });
  }

  ngOnInit() {
    this.noNewPlayer = true;
    this.gameService.getStart().subscribe((data) => {
      this.data = data;      
    });
  }
}
