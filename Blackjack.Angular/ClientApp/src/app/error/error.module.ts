import { NgModule } from "@angular/core";

import { ErrorRoutingModule } from "src/app/error/error-routing.module";
import { ErrorComponent } from "src/app/error/error/error.component";

@NgModule({
  imports: [ErrorRoutingModule],
  declarations: [ErrorComponent]
})

export class ErrorModule { }
