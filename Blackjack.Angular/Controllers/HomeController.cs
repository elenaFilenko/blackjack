﻿using System.Web.Mvc;

namespace Blackjack.Angular.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return new FilePathResult("~/ClientApp/dist/ClientApp/index.html", "text/html");
        }
    }
}