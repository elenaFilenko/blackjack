﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Blackjack.Entities.Entities;

namespace Blackjack.DataAccess.Interfaces
{
    public interface IGamePlayerRepository:IBaseRepository<GamePlayer>
    {
        Task<IEnumerable<GamePlayer>> GetWithoutDealerByGameId(long id);
        Task<IEnumerable<GamePlayer>> GetAll();
        Task<IEnumerable<GamePlayer>> GetAllByGameId(long id);
        Task<GamePlayer> GetPlayerByGameId(long id);
        Task<GamePlayer> GetDealerByGameId(long id);
    }
}
