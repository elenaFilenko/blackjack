﻿using Blackjack.Entities.Entities;

namespace Blackjack.DataAccess.Interfaces
{
    public interface ICardRepository:IBaseRepository<Card>{
    }
}
