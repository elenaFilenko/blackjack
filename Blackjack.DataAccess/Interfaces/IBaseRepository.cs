﻿using Blackjack.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blackjack.DataAccess.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        Task<IEnumerable<TEntity>> All();
        Task<TEntity> GetById(long id);
        Task Delete(TEntity entity);
        Task<long> Add(TEntity entity);
        Task Update(TEntity entity);
        Task Update(List<TEntity> entities);
        Task Add(List<TEntity> entities);
    }
}
