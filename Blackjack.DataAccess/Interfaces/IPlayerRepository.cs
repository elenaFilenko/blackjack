﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Blackjack.Entities.Entities;

namespace Blackjack.DataAccess.Interfaces
{
    public interface IPlayerRepository:IBaseRepository<Player>
    {
        Task<IEnumerable<Player>> GetPlayers();
        Task<IEnumerable<Player>> GetDealers();
        Task<IEnumerable<Player>> GetBots();       
    }
}
