﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Blackjack.Entities.Entities;

namespace Blackjack.DataAccess.Interfaces
{
    public interface IGameRepository:IBaseRepository<Game>
    {
        Task<IEnumerable<Game>> GetAll();
    }
}
