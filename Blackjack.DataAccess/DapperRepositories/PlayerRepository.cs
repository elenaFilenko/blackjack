﻿using System.Collections.Generic;
using Blackjack.DataAccess.Interfaces;
using Blackjack.Entities.Enums;
using Blackjack.Entities.Entities;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Blackjack.DataAccess.DapperRepositories
{
    public class PlayerRepository : BaseRepository<Player>, IPlayerRepository
    {
        private static string sqlQuery = "SELECT * FROM Players WHERE Role=@Role";

        public PlayerRepository(string connectionString):base(connectionString){
        }

        public async Task<IEnumerable<Player>> GetBots()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = await db.QueryAsync<Player>(sqlQuery, new { Role = PlayerRole.Bot });
                return result;
            }
        }

        public async Task<IEnumerable<Player>> GetDealers()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var dealers = await db.QueryAsync<Player>(sqlQuery, new { Role = PlayerRole.Dealer });
                return dealers;
            }
        }

        public async Task<IEnumerable<Player>> GetPlayers()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var players = await db.QueryAsync<Player>(sqlQuery, new { Role = PlayerRole.Player });
                return players;
            }
        }
    }
}

