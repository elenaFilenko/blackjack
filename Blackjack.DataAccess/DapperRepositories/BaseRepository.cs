﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Blackjack.DataAccess.Interfaces;
using Blackjack.Entities.Entities;
using Dapper.Contrib.Extensions;

namespace Blackjack.DataAccess.DapperRepositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        protected string _connectionString;

        public BaseRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<long> Add(TEntity entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                return await db.InsertAsync<TEntity>(entity);
            }
        }

        public async Task Add(List<TEntity> entities)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
               await db.InsertAsync(entities);
            }
        }

        public async Task<IEnumerable<TEntity>> All()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = await db.GetAllAsync<TEntity>();
                return result;
            }
        }

        public async Task Delete(TEntity entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                await db.DeleteAsync<TEntity>(entity);
            }
        }

        public async Task<TEntity> GetById(long id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                return await db.GetAsync<TEntity>(id);
            }
        }
        
        public async Task Update(TEntity entity)
        {
            using(IDbConnection db=new SqlConnection(_connectionString))
            {
                await db.UpdateAsync<TEntity>(entity);
            }
        }

        public async Task Update(List<TEntity> entities)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                await db.UpdateAsync(entities);
            }
        }
    }
}
