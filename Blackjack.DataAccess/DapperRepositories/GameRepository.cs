﻿using System.Collections.Generic;
using Blackjack.DataAccess.Interfaces;
using Blackjack.Entities.Entities;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using System.Linq;

namespace Blackjack.DataAccess.DapperRepositories
{
    public class GameRepository : BaseRepository<Game>, IGameRepository
    {
        public GameRepository(string connectionString):base(connectionString){
        }

        public async Task<IEnumerable<Game>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var gameDictionary = new Dictionary<long, Game>();
                string query = "SELECT * FROM Games AS A INNER JOIN GamePlayers AS B ON A.Id=B.GameId INNER JOIN Players AS C ON B.PlayerId=C.Id;";
                var list = (await db.QueryAsync<Game, GamePlayer, Player, Game>(query, (game, gamePlayer, player) => {
                    Game gameEntry;
                    if(!gameDictionary.TryGetValue(game.Id,out gameEntry))
                    {
                        gameEntry = game;
                        gameEntry.GamePlayers = new List<GamePlayer>();
                        gameDictionary.Add(gameEntry.Id, gameEntry);
                    }
                    gamePlayer.Player = player;
                    gameEntry.GamePlayers.Add(gamePlayer);
                    return gameEntry;
                })).Distinct().ToList();
                return list;
            }
        }
    }
}
