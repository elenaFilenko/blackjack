﻿using Blackjack.DataAccess.Interfaces;
using Blackjack.Entities.Entities;

namespace Blackjack.DataAccess.DapperRepositories
{
    public class CardRepository : BaseRepository<Card>, ICardRepository
    {
        public CardRepository(string connectionString):base(connectionString){        
        }
    }
}
