﻿using System.Collections.Generic;
using Blackjack.DataAccess.Interfaces;
using Blackjack.Entities.Enums;
using Blackjack.Entities.Entities;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Linq;
using Dapper.Mapper;

namespace Blackjack.DataAccess.DapperRepositories
{
    public class GamePlayerRepository:BaseRepository<GamePlayer>,IGamePlayerRepository
    {
        private string sqlQueryGetPlayerByGameId = "SELECT * FROM GamePlayers AS A INNER JOIN Players AS B ON A.PlayerId=B.Id WHERE A.GameId=@GameId AND B.Role=@Role;";

        public GamePlayerRepository(string connectionString):base(connectionString){
        }
        
        public async Task<GamePlayer> GetDealerByGameId(long id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = (await db.QueryAsync<GamePlayer, Player>(sqlQueryGetPlayerByGameId, new { GameId = id, Role = PlayerRole.Dealer })).FirstOrDefault();
                return result;
            }
        }
        
        public async Task<GamePlayer> GetPlayerByGameId(long id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = (await db.QueryAsync<GamePlayer,Player>(sqlQueryGetPlayerByGameId, new { GameId = id, Role = PlayerRole.Player })).FirstOrDefault();
                return result;
            }
        }

        public async Task<IEnumerable<GamePlayer>> GetAllByGameId(long id)
        {
            string query = "SELECT * FROM GamePlayers AS A INNER JOIN Players AS B ON A.PlayerId=B.Id INNER JOIN Games AS C ON A.GameId=C.Id WHERE A.GameId=@GameId;";
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var gamePlayers = (await db.QueryAsync<GamePlayer, Player, Game, GamePlayer>(query,
                    (gamePlayer, player, game) => {
                        gamePlayer.Player = player;
                        gamePlayer.Game = game;
                        return gamePlayer;
                    }, 
                    new {GameId=id })).ToList();
                return gamePlayers;
            }
        }

        public async Task<IEnumerable<GamePlayer>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connectionString)) {
                var gamePlayers = await db.QueryAsync<GamePlayer, Player, Game, GamePlayer>("SELECT * FROM GamePlayers AS A INNER JOIN Players AS B ON A.PlayerId=B.Id;", 
                    (gamePlayer, player, game) => {
                    gamePlayer.Player = player;
                    gamePlayer.Game = game;
                    return gamePlayer;
                });
                return gamePlayers;
            }
        }

        public async Task<IEnumerable<GamePlayer>> GetWithoutDealerByGameId(long id)
        {
            string query = "SELECT * FROM GamePlayers AS A INNER JOIN Players AS B ON A.PlayerId=B.Id INNER JOIN Games AS C ON A.GameId=C.Id WHERE A.GameId=@GameId AND B.Role!=@Role;";
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = (await db.QueryAsync<GamePlayer, Player, Game, GamePlayer>(query,
                    (gamePlayer, player, game) => {
                        gamePlayer.Player = player;
                        gamePlayer.Game = game;
                        return gamePlayer;
                    },
                    new { GameId = id, Role=PlayerRole.Dealer})).ToList();
                return result;
            }
        }
    }
}
