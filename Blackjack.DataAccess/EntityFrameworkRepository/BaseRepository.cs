﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Blackjack.DataAccess.Interfaces;
using Blackjack.Entities.Entities;

namespace Blackjack.DataAccess.EntityFrameworkRepository
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        protected BlackjackContext _context;

        public BaseRepository(BlackjackContext context)
        {
            _context= context;
        }

        public async Task<long> Add(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);           
            return await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<TEntity>> All()
        {
            var entityItems =await _context.Set<TEntity>().ToListAsync();
            return entityItems;
        }

        public async Task Delete(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
             await _context.SaveChangesAsync();
        }

        public async Task<TEntity> GetById(long id)
        {
            TEntity entity = await _context.Set<TEntity>().FindAsync(id);
            return entity;
        }

        public async Task Update(TEntity entity)
        {
             _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task Update(List<TEntity> entities)
        {
            foreach(TEntity entity in entities)
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
            await _context.SaveChangesAsync();
        }

        public async Task Add(List<TEntity> entities)
        {
            foreach (TEntity entity in entities)
            {
                _context.Set<TEntity>().Add(entity);
            }
            await _context.SaveChangesAsync();
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
