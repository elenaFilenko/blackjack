﻿using System.Collections.Generic;
using System.Linq;
using Blackjack.Entities.Enums;
using Blackjack.Entities.Entities;
using System.Data.Entity;
using System.Threading.Tasks;
using Blackjack.DataAccess.Interfaces;

namespace Blackjack.DataAccess.EntityFrameworkRepository
{
    public class PlayerRepository : BaseRepository<Player>, IPlayerRepository
    {
        public PlayerRepository(BlackjackContext context):base(context){
        }

        public async Task<IEnumerable<Player>> GetBots()
        {
            var bots = await _context.Players.Where(p => (p.Role.Equals(PlayerRole.Bot))).ToListAsync();
            return bots;
        }

        public async Task<IEnumerable<Player>> GetDealers()
        {
            var dealers = await _context.Players.Where(p => (p.Role.Equals(PlayerRole.Dealer))).ToListAsync();
            return dealers;
        }

        public async Task<IEnumerable<Player>> GetPlayers()
        {
            var players = await _context.Players.Where(p => (p.Role.Equals(PlayerRole.Player))).ToListAsync();
            return players;
        }
    }
}