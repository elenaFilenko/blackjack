﻿using System.Collections.Generic;
using Blackjack.Entities.Entities;
using System.Data.Entity;
using Blackjack.DataAccess.Interfaces;
using System.Threading.Tasks;

namespace Blackjack.DataAccess.EntityFrameworkRepository
{
    public class GameRepository :BaseRepository<Game>, IGameRepository
    {
        public GameRepository(BlackjackContext context):base(context){
        }

        public async Task<IEnumerable<Game>> GetAll()
        {
            var result = await _context.Games.Include(g => g.GamePlayers).ToListAsync();
            return result;
        }
    }
}