﻿using System.Collections.Generic;
using System.Linq;
using Blackjack.Entities.Enums;
using Blackjack.Entities.Entities;
using System.Data.Entity;
using Blackjack.DataAccess.Interfaces;
using System.Threading.Tasks;

namespace Blackjack.DataAccess.EntityFrameworkRepository
{
    public class GamePlayerRepository : BaseRepository<GamePlayer>, IGamePlayerRepository
    {
        public GamePlayerRepository(BlackjackContext context):base(context){
        }

        public async Task<GamePlayer> GetDealerByGameId(long id)
        {
            var gamePlayer = await _context.GamePlayers.Where(x=>(x.Player.Role.Equals(PlayerRole.Dealer))).Include(x=>x.Player).FirstOrDefaultAsync();
            return gamePlayer;
        }

        public async Task<GamePlayer> GetPlayerByGameId(long id)
        {
            var gamePlayer = await _context.GamePlayers.Where(x => (x.Player.Role.Equals(PlayerRole.Player))).Include(x=>x.Player).FirstOrDefaultAsync();
            return gamePlayer;
        }

        public async Task<IEnumerable<GamePlayer>> GetByGameId(long id)
        {
            var gamePlayers = await _context.GamePlayers.Where(x => (x.GameId == id)).ToListAsync();
            return gamePlayers;
        }

        public async Task<IEnumerable<GamePlayer>> GetAllByGameId(long id)
        {
            var gamePlayers = await _context.GamePlayers.Where(x => (x.GameId == id)).Include(x=>x.Player).ToListAsync();
            return gamePlayers;
        }

        public async Task<IEnumerable<GamePlayer>> GetAll()
        {
            var gamePlayers = await _context.GamePlayers.Include(gp => gp.Player).ToListAsync();
            return gamePlayers;
        }

        public async Task<IEnumerable<GamePlayer>> GetWithoutDealerByGameId(long id)
        {
            var gamePlayers = await _context.GamePlayers.Where(x => (x.GameId == id && !(x.Player.Role.Equals(PlayerRole.Dealer)))).ToListAsync();
            return gamePlayers;
        }
    }
}