﻿using Blackjack.Entities.Entities;
using Blackjack.DataAccess.Interfaces;

namespace Blackjack.DataAccess.EntityFrameworkRepository
{
    public class CardRepository : BaseRepository<Card>, ICardRepository
    {
        public CardRepository(BlackjackContext context):base(context){
        }
    }
}