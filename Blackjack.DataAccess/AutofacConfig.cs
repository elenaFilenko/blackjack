﻿using Autofac;
using Blackjack.DataAccess.DapperRepositories;
using Blackjack.DataAccess.Interfaces;

namespace Blackjack.DataAccess
{
    public static class AutofacConfig
    {
        public static void Configure(ContainerBuilder builder, string connectionString) {
            builder.RegisterGeneric(typeof(BaseRepository<>)).As(typeof(IBaseRepository<>)).WithParameter("connectionString", connectionString);
            builder.RegisterType<CardRepository>().As<ICardRepository>().WithParameter("connectionString", connectionString);
            builder.RegisterType<GameRepository>().As<IGameRepository>().WithParameter("connectionString", connectionString);
            builder.RegisterType<GamePlayerRepository>().As<IGamePlayerRepository>().WithParameter("connectionString", connectionString);
            builder.RegisterType<PlayerRepository>().As<IPlayerRepository>().WithParameter("connectionString", connectionString);
        }
    }
}
