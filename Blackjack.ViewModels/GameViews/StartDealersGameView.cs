﻿using System.Collections.Generic;

namespace Blackjack.ViewModels.GameViews
{
    public class StartDealersGameView
    {
        public List<PlayerStartDealersGameViewItem> DealersList { get; set;}

        public StartDealersGameView()
        {
            DealersList = new List<PlayerStartDealersGameViewItem>();
        }
    }

    public class PlayerStartDealersGameViewItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}