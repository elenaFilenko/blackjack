﻿using System;
using System.Collections.Generic;

namespace Blackjack.ViewModels.GameViews
{
    public class HistoryGameView
    {
        public List<GameHistoryGameViewItem> Games { get; set; }

        public HistoryGameView()
        {
            Games = new List<GameHistoryGameViewItem>();
        }
    }

    public class GameHistoryGameViewItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string DealerName { get; set; }
        public string PlayerName { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
