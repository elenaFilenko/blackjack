﻿using System.Collections.Generic;

namespace Blackjack.ViewModels.GameViews
{
    public class StartPlayersGameView
    {
        public List<PlayerStartPlayersGameViewItem> PlayersList { get; set; }

        public StartPlayersGameView()
        {
            PlayersList = new List<PlayerStartPlayersGameViewItem>();
        }
    }

    public class PlayerStartPlayersGameViewItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
