﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Blackjack.ViewModels.GameViews
{
    public class DetailsGameView
    {
        public long Id { get; set; }
        [Display(Name ="Game's name")]
        public string Name { get; set; }
        [Display(Name="Created")]
        public DateTime CreationDate { get; set; }
        [Display(Name ="Dealer")]
        public GamePlayerDetailsGameViewItem Dealer { get; set; }
        public List<GamePlayerDetailsGameViewItem> Players { get; set; }

        public DetailsGameView()
        {
            Dealer = new GamePlayerDetailsGameViewItem();
            Players = new List<GamePlayerDetailsGameViewItem>();
        }
    }

    public class GamePlayerDetailsGameViewItem
    {
        public long Id { get; set; }
        public long GameId { get; set; }
        public long PlayerId { get; set; }
        public string Name { get; set; }
        public int Points { get; set; }
        public string Result { get; set; }
    }
}
