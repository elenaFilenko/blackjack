﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Blackjack.ViewModels.GameViews
{
    public class PlayGameView
    {
        public long Id { get; set; }
        public string Name { get; set; }
        [Display(Name="Created")]
        public DateTime CreationDate { get; set; }
        [Display(Name ="Game dealer")]
        public GamePlayerPlayGameViewItem Dealer { get; set; }
        public List<GamePlayerPlayGameViewItem> Players { get; set; }

        public PlayGameView()
        {
            Dealer = new GamePlayerPlayGameViewItem();
            Players = new List<GamePlayerPlayGameViewItem>();
        }
    }

    public class GamePlayerPlayGameViewItem
    {
        public long Id { get; set; }
        public long GameId { get; set; }
        public long PlayerId { get; set; }
        public string Name { get; set; }
        public int Points { get; set; }
        public string Result { get; set; }
    }
}
