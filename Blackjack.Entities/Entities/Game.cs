﻿using Dapper.Contrib.Extensions;
using System.Collections.Generic;

namespace Blackjack.Entities.Entities
{
    public class Game:BaseEntity
    {
        public string Name { get; set; }

        [Write(false)]
        public virtual ICollection<GamePlayer> GamePlayers { get; set; }
    }
}