﻿using Blackjack.Entities.Enums;

namespace Blackjack.Entities.Entities
{
    public class Card:BaseEntity
    {
        public CardName CardName { get; set; }
        public CardSuit Suit { get; set; }
        public int Value { get; set; }
    }
}