﻿using System.Collections.Generic;
using Blackjack.Entities.Enums;
using Dapper.Contrib.Extensions;

namespace Blackjack.Entities.Entities
{
    public class Player:BaseEntity
    {
        public string Name { get; set; }
        public PlayerRole Role { get; set; }

        [Write(false)]
        public virtual ICollection<GamePlayer> GamePlayers { get; set; }
    }
}