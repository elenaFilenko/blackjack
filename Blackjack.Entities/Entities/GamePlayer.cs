﻿using Blackjack.Entities.Enums;
using Dapper.Contrib.Extensions;

namespace Blackjack.Entities.Entities
{
    public class GamePlayer:BaseEntity
    {
        public long GameId { get; set; }
        public long PlayerId { get; set; }
        public GameResult Result { get; set; }
        public int Points { get; set; }

        [Write(false)]
        public virtual Player Player { get; set; }
        [Write(false)]
        public virtual Game Game { get; set; }
    }
}