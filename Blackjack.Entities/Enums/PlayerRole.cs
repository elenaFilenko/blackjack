﻿namespace Blackjack.Entities.Enums
{
    public enum PlayerRole
    {
        Player =0,
        Bot = 1,
        Dealer = 2
    }
}
