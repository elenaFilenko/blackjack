﻿namespace Blackjack.Entities.Enums
{
    public enum GameResult
    {
        More=0,
        Enough=1,
        Blackjack=2,
        Lost=3,
        Even=4,
        Won=5
    }
}
