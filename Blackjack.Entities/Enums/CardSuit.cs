﻿namespace Blackjack.Entities.Enums
{
    public enum CardSuit
    {
        Heart=0,
        Diamond=1,
        Spade=2,
        Club=3
    }
}
