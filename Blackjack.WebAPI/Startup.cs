﻿using System.Configuration;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Blackjack.BusinessLogic;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Blackjack.WebAPI.Startup))]

namespace Blackjack.WebAPI
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            string connectionString = ConfigurationManager.ConnectionStrings["BlackJackDb"].ConnectionString;
            AutofacConfig.Configure(builder, connectionString);
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver((IContainer)container);
            app.UseAutofacMiddleware(container);
        }
    }
}
