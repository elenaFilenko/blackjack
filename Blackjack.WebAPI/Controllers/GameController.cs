﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Blackjack.BusinessLogic.Services.Interfaces;
using Blackjack.ViewModels.GameViews;
using NLog;

namespace Blackjack.WebAPI.Controllers
{
    [RoutePrefix("api/game")]
    public class GameController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private IGameService _gameService;
               
        public GameController(IGameService gameService)
        {
            _gameService= gameService;
        }

        [HttpGet]
        [Route("details/{id:long}")]
        public async Task<IHttpActionResult> Details(long id)
        {
            try
            {
                DetailsGameView game = await _gameService.Details(id);
                return Ok(game);
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return InternalServerError();
            }
        }

        [HttpGet]
        [Route("start")]
        public async Task<IHttpActionResult> Start()
        {
            try
            {
                StartGameView startGameView = new StartGameView();
                startGameView = await _gameService.Start();
                return Ok(startGameView);
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return InternalServerError();
            }
        }

        [HttpPost]
        [Route("start")]
        public async Task<IHttpActionResult> Start([FromBody]StartGameView startGameView)
        {
            try
            {
                var gameId = await _gameService.Start(startGameView);
                return Ok(gameId);
            }
            catch(Exception e)
            {
                logger.Error(e.Message);
                return InternalServerError();
            }
        }

        [HttpGet]
        [Route("play/{id:long}")]
        public async Task<IHttpActionResult> Play(long id)
        {
            try
            {
                PlayGameView playGameView = new PlayGameView();
                playGameView = await _gameService.Play(id);
                
                if (playGameView != null)
                {
                    return Ok(playGameView);
                }
                return NotFound();
            }
            catch(Exception e)
            {
                logger.Error(e.Message);
                return InternalServerError();
            }
        }

        [HttpPut]
        [Route("enough/{id:long}")]
        public async Task<IHttpActionResult> Enough(long id)
        {
            try
            {
                await _gameService.Enough(id);
                return Ok();
            }
            catch(Exception e)
            {
                logger.Error(e.Message);
                return InternalServerError();
            }
        }
       
        [HttpPut]
        [Route("more/{id:long}")]
        public async Task<IHttpActionResult> More(long id)
        {
            try
            {
                await _gameService.More(id);
                return Ok();
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return InternalServerError();
            }
        }

        [HttpGet]
        [Route("history")]
        public async Task<IHttpActionResult> History()
        {
            try
            {
                HistoryGameView history = await _gameService.History();
                return Ok(history);
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return InternalServerError();
            }
        }
    }
}
